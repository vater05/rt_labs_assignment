package com.filippovsky;

import junit.framework.TestCase;

/**
 * Created by Yan Filippovsky (yan.filippovsky@yandex.ru)
 * on 16.08.17.
 */
public class DocumentTest extends TestCase {

    static final String text =
            "- Не может быть, - проговорил он тихо и раздельно.\n" +
            "- Что, Бруно, не может быть?\n" +
            "- Ах, пустые мысли, - ответил он угрюмо, - пустые мысли.\n";

    public void testLoadFromString() throws Exception {

        Document doc = Document.loadFromString(text);
        assertEquals(3, doc.getSentences().size());
    }
}
