package com.filippovsky;

import junit.framework.TestCase;

/**
 * Created by Yan Filippovsky (yan.filippovsky@yandex.ru)
 * on 16.08.17.
 */
public class MergeSorterTest extends TestCase {

    private static MergeSorter asc = new MergeSorter(MergeSorter.Direction.Asc);
    private static MergeSorter desc = new MergeSorter(MergeSorter.Direction.Desc);

    public void testSort_sorted() throws Exception {

        Integer[] a = {1, 2, 3};
        asc.sort(a);
        assertEquals(1, (int) a[0]);
        assertEquals(2, (int) a[1]);
        assertEquals(3, (int) a[2]);
    }

    public void testSort_asc() throws Exception {

        Integer[] a = {2, 1, 3};
        asc.sort(a);
        assertEquals(1, (int) a[0]);
        assertEquals(2, (int) a[1]);
        assertEquals(3, (int) a[2]);
    }

    public void testSort_desc() throws Exception {

        Integer[] a = {2, 1, 3};
        desc.sort(a);
        assertEquals(3, (int) a[0]);
        assertEquals(2, (int) a[1]);
        assertEquals(1, (int) a[2]);
    }

    public void testSort_nonUnique() throws Exception {

        Integer[] a = {2,3,4,3,2};
        asc.sort(a);
        assertEquals(2, (int) a[0]);
        assertEquals(2, (int) a[1]);
        assertEquals(3, (int) a[2]);
        assertEquals(3, (int) a[3]);
        assertEquals(4, (int) a[4]);
    }
}
