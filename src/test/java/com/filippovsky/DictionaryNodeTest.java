package com.filippovsky;

import junit.framework.TestCase;

/**
 * Created by Yan Filippovsky (yan.filippovsky@yandex.ru)
 * on 15.08.17.
 */
public class DictionaryNodeTest extends TestCase {

    public void testHasChild() throws Exception {

        DictionaryNode node = new DictionaryNode("");
        node.addCompoundWord("а-ля");

        assertTrue(node.hasChild("а-ля"));
        assertTrue(node.hasChild("А-ЛЯ"));
        assertTrue(node.hasChild("  а-ля "));
        assertFalse(node.hasChild("а - ля"));
    }

    public void testGetChild() throws Exception {

        DictionaryNode node = new DictionaryNode("");
        node.addCompoundWord("а-ля");

        assertTrue(node.getChild("а-ля") == node.getChild("А-ЛЯ"));
        assertTrue(node.getChild("а-ля") == node.getChild("  а-ля "));
        assertNull(node.getChild("а - ля"));
    }

    public void testAddCompoundWord_empty() throws Exception {

        DictionaryNode node = new DictionaryNode("в");
        node.addCompoundWord("");
        node.addCompoundWord("   ");

        assertEquals(0, node.getChildNum());
    }

    public void testAddCompoundWord_single() throws Exception {

        DictionaryNode node = new DictionaryNode("в");
        node.addCompoundWord("виде");

        assertEquals(1, node.getChildNum());

        DictionaryNode child = node.getChild("виде");
        assertNotNull(child);
        assertEquals("виде", child.getBaseWord());
    }

    public void testAddCompoundWord_exists() throws Exception {

        DictionaryNode node = new DictionaryNode("в");
        node.addCompoundWord("виде");
        node.addCompoundWord("виде");
        node.addCompoundWord("ВИДЕ");

        assertEquals(1, node.getChildNum());

        DictionaryNode child = node.getChild("виде");
        assertNotNull(child);
        assertEquals("виде", child.getBaseWord());
    }

    public void testAddCompoundWord_compound() throws Exception {

        DictionaryNode node = new DictionaryNode("несмотря");
        node.addCompoundWord("ни на что");

        assertEquals(1, node.getChildNum());

        node = node.getChild("ни");
        assertNotNull(node);
        assertEquals("ни", node.getBaseWord());
        assertEquals(1, node.getChildNum());

        node = node.getChild("на");
        assertNotNull(node);
        assertEquals("на", node.getBaseWord());
        assertEquals(1, node.getChildNum());

        node = node.getChild("что");
        assertNotNull(node);
        assertEquals("что", node.getBaseWord());
        assertEquals(0, node.getChildNum());
    }

    public void testAddCompoundWord_multiple() throws Exception {

        DictionaryNode node = new DictionaryNode("");
        node.addCompoundWord("в");
        node.addCompoundWord("в виде");
        node.addCompoundWord("в течение");

        assertEquals(1, node.getChildNum());

        node = node.getChild("в");
        assertNotNull(node);
        assertEquals("в", node.getBaseWord());
        assertEquals(2, node.getChildNum());

        DictionaryNode child = node.getChild("виде");
        assertNotNull(child);
        assertEquals("виде", child.getBaseWord());
        assertEquals(0, child.getChildNum());

        child = node.getChild("течение");
        assertNotNull(child);
        assertEquals("течение", child.getBaseWord());
        assertEquals(0, child.getChildNum());
    }
}
