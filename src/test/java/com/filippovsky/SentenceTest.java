package com.filippovsky;

import junit.framework.TestCase;

import java.util.List;

/**
 * Created by Yan Filippovsky (yan.filippovsky@yandex.ru)
 * on 15.08.17.
 */
public class SentenceTest extends TestCase {

    public void testParse_regular() throws Exception {

        Sentence sentence = new Sentence("Нет-нет, - сказал он сонно.");
        List<String> words = sentence.getWords();

        assertEquals("нет-нет", words.get(0));
        assertEquals("сказал", words.get(1));
        assertEquals("он", words.get(2));
        assertEquals("сонно", words.get(3));
    }
}
