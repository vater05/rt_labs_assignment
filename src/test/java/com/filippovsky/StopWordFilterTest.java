package com.filippovsky;

import junit.framework.TestCase;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Yan Filippovsky (yan.filippovsky@yandex.ru)
 * on 14.08.17.
 */
public class StopWordFilterTest extends TestCase {

    static final String stopWords = "а-ля\n" +
            "в\n" +
            "в виде\n" +
            "в течение\n" +
            "по\n" +
            "якобы\n";

    public void testFilter_noStopWords() throws Exception {

        StopWordFilter filter = new StopWordFilter();
        filter.loadStopWordsFromString(stopWords);

        List<String> w = Arrays.asList("Он", "пришёл", "раньше", "намеченного");
        List<String> match = filter.filter(w);

        assertEquals(4, match.size());
        assertEquals("он", match.get(0));
        assertEquals("пришёл", match.get(1));
        assertEquals("раньше", match.get(2));
        assertEquals("намеченного", match.get(3));
    }

    public void testFilter_stopWordsOnly() throws Exception {

        StopWordFilter filter = new StopWordFilter();
        filter.loadStopWordsFromString(stopWords);

        List<String> w = Arrays.asList("В", "течение", "а-ля", "якобы", "в", "виде");
        List<String> match = filter.filter(w);

        assertEquals(0, match.size());
    }

    public void testFilter() throws Exception {

        StopWordFilter filter = new StopWordFilter();
        filter.loadStopWordsFromString(stopWords);

        List<String> w = Arrays.asList("В", "течение", "продолжительного", "времени", "он", "по", "обыкновению", "молчал");
        List<String> match = filter.filter(w);

        assertEquals(5, match.size());
        assertEquals("продолжительного", match.get(0));
        assertEquals("времени", match.get(1));
        assertEquals("он", match.get(2));
        assertEquals("обыкновению", match.get(3));
        assertEquals("молчал", match.get(4));
    }
}
