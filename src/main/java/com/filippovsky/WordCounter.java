package com.filippovsky;

import java.util.*;

/**
 * Counts a number of occurrences of each word in the document.
 * Uses StopWordFilter to discard stop-words.
 */
class WordCounter {

    private StopWordFilter stopWordsFilter;

    WordCounter(StopWordFilter stopWordsFilter) {
        this.stopWordsFilter = stopWordsFilter;
    }

    /**
     * Counts a number of occurrences of each essential word
     * in the specified document discarding any stop-words
     * specified by {@link WordCounter#stopWordsFilter} member.
     *
     * @param document a document to count occurrences
     * @return a map in which the key represents a word,
     * and the value stores the number of occurrences of the given word
     */
    Map<String, Integer> count(Document document) {

        Map<String, Integer> stats = new HashMap<>();

        for (Sentence sentence: document.getSentences()) {
            List<String> words = stopWordsFilter.filter(sentence.getWords());

            for (String word: words) {
                stats.merge(word, 1, Integer::sum); /* increment or set the value */
            }
        }
        return stats;
    }

    /**
     * Counts a number of occurrences of each word in the document.
     * Similar to {@link WordCounter#count} method, counts the occurrences
     * of each word, but reverses the mapping. Each key of the returning map
     * represents a number of occurrences as well as the value holds
     * the words with a given number of occurrences.
     *
     * @param document a document to count occurrences
     * @return a map containing words grouped by number of occurrences
     */
    Map<Integer, List<String>> countReverse(Document document) {

        Map<String, Integer> stats = count(document);
        Map<Integer, List<String>> reverseStats = new HashMap<>();

        stats.forEach((word, count) -> {
            reverseStats.computeIfAbsent(count, words -> new LinkedList<>()).add(word);
        });

        return reverseStats;
    }
}
