package com.filippovsky;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Classic merge sort algorithm implementation
 */
public class MergeSorter {

    enum Direction {
        Asc,
        Desc;
    }

    private Direction dir = Direction.Asc;

    MergeSorter(Direction dir) {
        this.dir = dir;
    }

    private int compare(Comparable a, Comparable b) {

        if (dir == Direction.Asc) {
            return a.compareTo(b);
        } else {
            return -a.compareTo(b);
        }
    }

    private void merge(Comparable[] list, Comparable[] buf, int lo, int mid, int hi) {

        /*
         * copy the elements from lo to hi (inclusive)
         * into the auxiliary array
         */
        System.arraycopy(list, lo, buf, lo, hi - lo + 1);

        int i = lo;
        int j = mid + 1;

        for (int k = lo; k <= hi; ++k) {

            if (i > mid) {              /* left part is over */
                buf[k] = list[j++];

            } else if (j > hi) {        /* right part is over */
                buf[k] = list[i++];

            } else if (compare(list[i], list[j]) <= 0) {
                buf[k] = list[i++];

            } else {
                buf[k] = list[j++];
            }
        }
        /*
         * put back the left and right parts merged together
         */
        System.arraycopy(buf, lo, list, lo, hi - lo + 1);
    }

    private void sort(Comparable[] list, Comparable[] buf, int lo, int hi) {

        if (hi <= lo)
            return;

        int mid = (lo + hi) / 2;

        /*
         * sort the left and right parts recursively
         */
        sort(list, buf, lo, mid);
        sort(list, buf, mid + 1, hi);
        merge(list, buf, lo, mid, hi);
    }

    /**
     * Inplace sort
     * @param list an array of non-null elements to sort
     */
    void sort(Comparable[] list) {

        if (list == null)
            return;

        Comparable[] buf = new Comparable[list.length];
        sort(list, buf, 0, list.length - 1);
    }
}
