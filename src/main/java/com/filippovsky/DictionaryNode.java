package com.filippovsky;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Represents a part of compound stop-word.
 *
 * Each node may have child nodes that represent different variations
 * of the parent node/word. For example compound conjunctions "as well as",
 * "as soon as", "as long as" has the word "as" as their common parent.
 * In that case child nodes of the base node "as" will have "well", "soon"
 * and "long" as their own children. Take a look at the diagram
 *
 *         as
 *       / | \
 *      /  |  \
 *   well soon long
 *    |    |    |
 *    as   as   as
 */
class DictionaryNode {

    private String baseWord;
    private Map<String, DictionaryNode> children;

    DictionaryNode(String baseWord) {
        this.baseWord = baseWord;
    }

    private void insertChild(String baseWord) {

        /* TODO: replace by computeIfAbsent */

        if (children == null)
            children = new HashMap<>();
        baseWord = baseWord.trim().toLowerCase();
        children.put(baseWord, new DictionaryNode(baseWord));
    }

    private void addCompoundWord(String[] parts) {

        if (! hasChild(parts[0])) {
            insertChild(parts[0]);
        }
        if (parts.length > 1) {
            DictionaryNode child = children.get(parts[0]);
            parts = Arrays.copyOfRange(parts, 1, parts.length);
            child.addCompoundWord(parts);
        }
    }

    /**
     * Builds a chain of child nodes containing parts of the compound word
     * @param compoundWord a compound word needs to be
     */
    void addCompoundWord(String compoundWord) {

        if (compoundWord == null)
            return;

        compoundWord = compoundWord.trim().toLowerCase();
        if (compoundWord.isEmpty())
            return;

        String[] parts = compoundWord.split("\\s+");
        addCompoundWord(parts);
    }

    String getBaseWord() {
        return baseWord;
    }

    DictionaryNode getChild(String baseWord) {
        return children != null ? children.get(baseWord.trim().toLowerCase()) : null;
    }

    int getChildNum() {
        return children != null ? children.size() : 0;
    }

    boolean hasChild(String baseWord) {
        return children != null && children.containsKey(baseWord.trim().toLowerCase());
    }
}
