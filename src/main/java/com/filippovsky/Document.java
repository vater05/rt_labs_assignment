package com.filippovsky;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Represents a written text.
 * Actually holds a list of sentences.
 *
 * Uses simple regular expressions to break text into sentences.
 *
 * It does not recognize parentheses, so that statements
 * inside parentheses may not be interpreted correctly.
 */
class Document {

    private List<Sentence> sentences;

    private Document() {
        sentences = new ArrayList<>();
    }

    private static Document readDocument(Reader reader) {

        Document doc = new Document();
        Scanner scanner = new Scanner(new BufferedReader(reader));
        scanner.useDelimiter("[.!?]+");

        while (scanner.hasNext()) {
            String sentence = scanner.next().trim();
            if (! sentence.isEmpty()) {
                doc.sentences.add(new Sentence(sentence));
            }
        }
        return doc;
    }

    /**
     * Reads a document from a string
     * @param text a string representing a written text
     */
    static Document loadFromString(String text) {
        Document doc = new Document();
        return readDocument(new StringReader(text));
    }

    /**
     * Reads a document from a file
     * @param fileName a path to input file containing a text
     * @throws IOException
     */
    static Document loadFromFile(String fileName) throws IOException {
        Document doc = new Document();
        try (Reader reader = new FileReader(fileName)) {
            return readDocument(reader);
        }
    }

    List<Sentence> getSentences() {
        return sentences;
    }
}
