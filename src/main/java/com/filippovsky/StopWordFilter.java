package com.filippovsky;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

/**
 * Used to extract essential words discarding unnecessary stop-words.
 *
 * Uses a tree data structure for storing all possible nonessential
 * words (conjunctions, prepositions, particles, etc.) that are
 * unnecessary for frequency analysis of the text.
 */
class StopWordFilter {

    private DictionaryNode root = new DictionaryNode("");

    /**
     * Reads a list of stop-words from a string.
     * List of words must be separated by a newline character.
     * Compound conjunctions, prepositions and particles are allowed.
     * @param stopWords string representing a list of stop-words
     */
    void loadStopWordsFromString(String stopWords) {

        if (stopWords != null) {
            stopWords = stopWords.trim();
            if (! stopWords.isEmpty()) {

                String[] split = stopWords.split(System.lineSeparator());
                Arrays.stream(split).forEach(w -> root.addCompoundWord(w));
            }
        }
    }

    /**
     * Reads a list of stop-words from a file.
     * @see StopWordFilter#loadStopWordsFromString
     * @param fileName a path to file needs to be load
     * @throws IOException
     */
    void loadStopWordsFromFile(String fileName) throws IOException {

        Path path = new File(fileName).toPath();
        Files.lines(path).forEach(w -> root.addCompoundWord(w));
    }

    /**
     * Filters the list of words (a sentence), leaving only the words that matter.
     * @param words input word list needs to filter
     * @return a word list that does not contain stop-words
     */
    List<String> filter(List<String> words) {

        List<String> matchWords = new ArrayList<>();

        if (words == null || words.isEmpty())
            return matchWords;

        Iterator<String> it = words.iterator();
        DictionaryNode node = root;

        while (it.hasNext()) {

            String word = it.next().trim().toLowerCase();
            DictionaryNode nextNode = node.getChild(word);

            if (nextNode == null) {
                node = root;
                nextNode = node.getChild(word);

                if (nextNode == null) {
                    matchWords.add(word);
                    continue;
                }
            }
            node = nextNode;
        }

        return matchWords;
    }
}
