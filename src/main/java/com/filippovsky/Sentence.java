package com.filippovsky;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Represents a sentence of written texts
 */
class Sentence {

    private List<String> words;

    /**
     * Construct a sentence from string.
     * Uses regular expressions to skip sequences of
     * spaces and punctuation (except an hyphen/dash "-" symbol).
     * Free-standing hyphen have filtered by stream's filter function.
     * @param sentence a string representing a sentence
     */
    Sentence(String sentence) {
        sentence = sentence.trim().toLowerCase();
        String[] split = sentence.split("[\\s\\p{Punct}&&[^-—]]+");
        words = Arrays.stream(split).filter(s -> !s.matches("[-—]+")).collect(Collectors.toList());
    }

    List<String> getWords() {
        return words;
    }
}
