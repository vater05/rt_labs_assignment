package com.filippovsky;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

public class App {

    /**
     * Represents a group of words which have the
     * same number of occurrences in the text
     */
    private static class Basket implements Comparable<Basket> {

        int count = 0;
        String[] words;

        Basket(int count, String[] words) {
            this.count = count;
            this.words = words;
        }

        @Override
        public int compareTo(Basket another) {
            return this.count - another.count;
        }
    }

    public static void main(String[] args) throws IOException {

        Document doc = Document.loadFromFile("in.txt");

        StopWordFilter filter = new StopWordFilter();
        filter.loadStopWordsFromFile("stop-words.txt");

        WordCounter counter = new WordCounter(filter);
        Map<Integer, List<String>> reverseStats = counter.countReverse(doc);

        /*
         * Convert the map containing the words grouped by
         * number of occurrences into an array of "baskets"
         * that have all the information needed to sort
         * the words by number of occurrences.
         */
        Basket[] baskets = reverseStats.entrySet().stream().map(e -> {

            String[] words = e.getValue().toArray(new String[0]); /* this is for sort needs */
            return new Basket(e.getKey(), words);

        }).toArray(Basket[]::new);

        /*
         * Sort the "baskets" then print out the word stats
         */
        MergeSorter asc = new MergeSorter(MergeSorter.Direction.Asc);
        MergeSorter desc = new MergeSorter(MergeSorter.Direction.Desc);
        desc.sort(baskets);

        try (FileWriter file = new FileWriter("out.txt")) {
            PrintWriter out = new PrintWriter(file);

            for (Basket b: baskets) {
                asc.sort(b.words);      /* sort words inside each group */

                for (String w: b.words) {
                    out.println(w + " " + b.count);
                }
            }
        }
    }
}
